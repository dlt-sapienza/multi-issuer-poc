from algosdk.v2client import algod 
from algosdk import account, encoding, constants
from algosdk import mnemonic
from algosdk.future import transaction
import json
import base64
import sys
from util import *


import algosdk
from algosdk.future.transaction import Multisig, PaymentTxn, MultisigTransaction, AssetConfigTxn, AssetTransferTxn, AssetFreezeTxn

endpoint_address = 'https://testnet-algorand.api.purestake.io/ps2'
purestake_key = 'F9I45UDrFb9FuoYhohD35A2Tcfq0mnZ5cTuG81x9'
purestake_header = {'X-Api-key': purestake_key}

sk_cmo_2, addr_cmo_2 = account.generate_account()
# print(multisig.address())
print(addr_cmo_2)
print(sk_cmo_2)

sk = 'hW/zDwfI1hw3iGKAT25vB3lf1Yh7lnBirhwzArtFNfurym4ttGkmdIo0SuCfvK58YacESnaw+n1IhoXBMfGNrA=='
addr = 'VPFG4LNUNETHJCRUJLQJ7PFOPRQ2OBCKO2YPU7KIQ2C4CMPRRWWJ6FJBUM'

addr_cmo = 'ZBA5BA2KWOWC7VRJQZH5PNRJBP3RU23MWQ4LPZ7KUQJ72PYE7NRHRSAW3A'
sk_cmo = 'HHFV+Fi+mV4rmUE6Zc9AS3mWrXx1nLMtw/cZGA3gy1fIQdCDSrOsL9Yphk/XtikL9xprbLQ4t+fqpBP9PwT7Yg=='

addr_cmo_2 = 'PP3GJSDXX66I2HQHBZY6P7FRF4ZTWCXWEHZ5WUETX3B5RUG6RE25SZIUME'
sk_cmo_2 = 'jg+5Gw1h2dzQEX/dEYZ1TWXunHK4/EiqyHyTtq/ZtQh79mTId7+8jR4HDnHn/LEvMzsK9iHz21CTvsPY0N6JNQ=='

addr_cmo_3 = '4FK3JYLUNTS4QQZGZX3SRWVP7JVOWY6Q2RJXQGJJISRZ2YNNUMJAKXH4UY'
sk_cmo_3 = 'cJ8qMM4Z5s2x0MrbnPKOPM2ARFrPfs0MjsGz3/hD05vhVbThdGzlyEMmzfco2q/6autj0NRTeBkpRKOdYa2jEg=='

multisig = create_multisig(2, [addr_cmo, addr_cmo_2])
print(multisig.address())

if __name__ == "__main__":
    acl = algod.AlgodClient(purestake_key, endpoint_address, headers=purestake_header)
    
    print("============== DEPLOY THE MULTIISSUER CONTRACT ==================")
    txn = generate_deploy_local_smart_contract(acl, addr_cmo,  './contract/local_contract_stateful.teal', './contract/local_contract_clear_state.teal' )
    response = send_unsigned_single_transaction(acl, txn, sk_cmo)
    appid = response['application-index']
    print("Application index is:", appid)

    print("\n\n============== ADD A NEW ISSUER ==================")
    txn_1 = generate_sc_optin_transaction(acl,addr_cmo_2, appid )
    txn_2 = generate_payment_transaction(acl, addr_cmo, addr_cmo, addr_cmo_2, 0)

    # get group id and assign it to transactions
    gid = transaction.calculate_group_id([txn_1, txn_2])
    txn_1.group = gid
    txn_2.group = gid

    # sign transactions
    stxn_1 = txn_1.sign(sk_cmo_2)    
    stxn_2 = txn_2.sign(sk_cmo)
    
    # assemble transaction group
    signed_group =  [stxn_1, stxn_2]
    try:
        send_signed_transaction(acl, signed_group)

        print(f"\n\n============== LOCAL STATE OF {addr_cmo_2} ==================")
        print_local_state(addr_cmo_2, acl, appid)
        
        print(f"\n\n============== TURN THE MANAGER INTO A MULTISIGNATURE 2/2 ==================")
        #create multisig here
        multisig = create_multisig(2, [addr_cmo, addr_cmo_2])
        print("Multisignature address is:", multisig.address())

        enc_args = []
        enc_args.append(str.encode("update"))
        enc_args.append(encoding.decode_address( multisig.address() ))
        
        txn = generate_sc_call_with_args_transaction(acl, addr_cmo, appid, enc_args)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo)
    
    except Exception as err:
        print("Error: {0}".format(err))

        print(f"\n\n============== OPTOUT OF {addr_cmo_2} ==================")
        txn = generate_sc_optout_transaction(acl, addr_cmo_2, appid)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo_2)
        
        print("\n\n============== DELETE THE CONTRACT ==================")
        txn = generate_delete_app(acl, addr_cmo, appid)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo)
        print(response)

    try:
        print(f"\n\n============== SWITCH STATE OF CMO2 ==================")

        txn = generate_sc_optin_transaction(acl, multisig.address(), appid)
        # create a MultisigTx object
        mtx = MultisigTransaction(txn, multisig)

        # sign the transaction
        mtx.sign(sk_cmo)
        mtx.sign(sk_cmo_2)
        
        send_multisigned_transaction(acl, mtx)

        enc_args = []
        enc_args.append(str.encode("switch"))
        
        enc_accounts = []
        enc_accounts.append(addr_cmo_2)

        txn = generate_sc_call_with_external_account_args_transaction(acl, multisig.address(), appid, enc_args, enc_accounts)
        
        # create a MultisigTx object
        mtx = MultisigTransaction(txn, multisig)

        # sign the transaction
        mtx.sign(sk_cmo)
        mtx.sign(sk_cmo_2)
        
        send_multisigned_transaction(acl, mtx)

        print(f"\n\n============== LOCAL STATE OF {addr_cmo_2} ==================")
        print_local_state(addr_cmo_2, acl, appid)


        print("\n\n============== ADD A NEW ISSUER ==================")
        txn_1 = generate_sc_optin_transaction(acl,addr_cmo_3, appid )
        txn_2 = generate_payment_transaction(acl, multisig.address(), multisig.address(), "", 0)

        # get group id and assign it to transactions
        gid = transaction.calculate_group_id([txn_1, txn_2])
        txn_1.group = gid
        txn_2.group = gid

        # create a Multisignature object
        mtx = MultisigTransaction(txn_2, multisig)
        mtx.group = gid

        # sign the second transaction
        mtx.sign(sk_cmo)
        mtx.sign(sk_cmo_2)
    
        # sign the first transaction
        stxn_1 = txn_1.sign(sk_cmo_3)    
        
        # assemble transaction group
        signed_group =  [stxn_1, mtx]
        
        send_signed_transaction(acl, signed_group)

        print(f"\n\n============== LOCAL STATE OF {addr_cmo_3} ==================")
        print_local_state(addr_cmo_3, acl, appid)

        print(f"\n\n============== OPTOUT OF {addr_cmo_3} ==================")
        txn = generate_sc_optout_transaction(acl, addr_cmo_3, appid)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo_3)

        print(f"\n\n============== OPTOUT OF {addr_cmo_2} ==================")
        txn = generate_sc_optout_transaction(acl, addr_cmo_2, appid)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo_2)


        print("\n\n============== DELETE THE CONTRACT ==================")
        txn = generate_delete_app(acl, multisig.address(), appid)

        # create a SignedTransaction object
        mtx = MultisigTransaction(txn, multisig)

        # sign the transaction
        mtx.sign(sk_cmo)
        mtx.sign(sk_cmo_2)
        
        response = send_multisigned_transaction(acl, mtx)
        print(response)
    except Exception as err:
        print("Error: {0}".format(err))

        print(f"\n\n============== OPTOUT OF {addr_cmo_2} ==================")
        txn = generate_sc_optout_transaction(acl, addr_cmo_2, appid)
        response = send_unsigned_single_transaction(acl, txn, sk_cmo_2)
        
        print("\n\n============== DELETE THE CONTRACT ==================")
        txn = generate_delete_app(acl, multisig.address(), appid)

        # create a Multisignature object
        mtx = MultisigTransaction(txn, multisig)

        # sign the transaction
        mtx.sign(sk_cmo)
        mtx.sign(sk_cmo_2)
        
        response = send_multisigned_transaction(acl, mtx)
        print(response)