from pyteal import *


def approval_program(): 
    on_creation = Seq([
        # Set the first manager as Txn.sender()
        Assert(Txn.application_args.length() == Int(0)),
        App.globalPut(Bytes("manager"), Txn.sender()),
        Return(Int(1)) 
    ])
    
    
    manager = App.globalGet(Bytes("manager"))
    
    #Routine to update the manager
    on_manager_update = Seq([
        # Check that the current manager is actually changing the manager
        Assert(Txn.sender() == manager),

        #The new manager is passed as the second argument
        Assert(Txn.application_args.length() == Int(2)),
        App.globalPut(Bytes("manager"), Txn.application_args[1]),

        Return(Int(1))
    ])
    
    #Routine to add a new trusted CMO addr
    on_add_trusted_addr = Seq([

        If(Gtxn[0].sender() == manager,  
            App.localPut(Int(0), Bytes("State"), Int(1)), # IF, simple optin of the manager
            Seq([Assert( # ELSE, checks to verify that the new issuer in enabled by the manager
                And(
                    Global.group_size() == Int(2),
                    Gtxn[1].sender() == manager,
                    Gtxn[1].type_enum() == Int(1),
                    Gtxn[1].amount() == Int(0),
                    )
                ),

            App.localPut(Int(0), Bytes("State"), Int(1)),
            ])),
        
        Return(Int(1))
    ])
    
    
    cmo = App.localGetEx(Int(1), Int(0), Bytes("State"))

    #Routine to disable/enable an addr
    on_switch_trusted_addr = Seq([
        
        cmo,
        Assert(Txn.sender() == manager),
        Assert(Txn.application_args.length() == Int(1)),
        Assert(cmo.hasValue()), # Check if the issuer is included in the contract

        # Switch the state of the local variable according to the current state
        If(cmo.value() == Int(1), App.localPut(Int(1), Bytes("State"), Int(0)), App.localPut(Int(1), Bytes("State"), Int(1))),
        Return(Int(1))
    ])
    
    '''Determine and validate which operation is executed by the Txn that invoked the contract'''
    txn_operation = Cond(
        [Txn.application_id() == Int(0), on_creation], #invoke app creation routine
        
        
        [Txn.on_completion() == OnComplete.DeleteApplication or #only the manager can delete the app or update its code
         Txn.on_completion() == OnComplete.UpdateApplication, Return(Txn.sender() == App.globalGet(Bytes("manager")))],
        
        [Gtxn[0].on_completion() == OnComplete.OptIn, on_add_trusted_addr], #opt-in function 
        [Txn.on_completion() == OnComplete.CloseOut, Return(Int(1))], #opt-out enabled
        
        [Txn.application_args[0] == Bytes("update"), on_manager_update], 
        [Txn.application_args[0] == Bytes("switch"), on_switch_trusted_addr]
     )

    
    program = txn_operation
    return program        
    
    
def clear_state_program(): #delete local storage and opt-out the application
    return Return(Int(1)) #opt-out enabled
    
if __name__ == "__main__":
    with open('./contract/local_contract_stateful.teal', 'w') as f:
        compiled = compileTeal(approval_program(), mode=Mode.Application, version=3)
        f.write(compiled)

    with open('./contract/local_contract_clear_state.teal', 'w') as f:
        compiled = compileTeal(clear_state_program(), mode=Mode.Application, version=3)
        f.write(compiled)