from algosdk.v2client import algod 
from algosdk import account, encoding, constants
from algosdk.future import transaction
import json
import base64
import sys
from util import *

import algosdk



if __name__ == "__main__":
    pass

# Utility function to wait for a transaction to be confirmed by network

def wait_for_tx_confirmation(acl, txid):
    last_round = acl.status()["last-round"]
    while True:
        txinfo = acl.pending_transaction_info(txid)
        
        if txinfo.get('confirmed-round') and txinfo.get('confirmed-round') > 0:
            print("Transaction {} confirmed in round {}.".format(txid, txinfo.get('confirmed-round')))
            break
        else:
            print("Waiting for confirmation..." + str(last_round))
            last_round += 1
            acl.status_after_block(last_round)

def compileProgram(acl, compiled_teal):
        #compile to binary
    compile_response = acl.compile(compiled_teal)
    compiled_binary = base64.b64decode(compile_response['result'])
    print("size of the compiled approval program: "+ repr(sys.getsizeof(compiled_binary)) + " bytes. \n"\
         + "Max allowed size 1000 bytes (including args)")
    
    return compiled_binary


def generate_deploy_local_smart_contract(acl, addr, approval_file, clear_file):
    # Deploy the contract
    sender = addr

    # get node suggested parameters
    params = acl.suggested_params()

    # declare on_complete as NoOp perché?
    on_complete = transaction.OnComplete.NoOpOC.real

    #At least one local and one global variable are necessary. Otherwise an error is triggered.
    local_ints = 1
    local_bytes = 0
    global_ints = 0
    global_bytes = 1

    # define schema
    global_schema = transaction.StateSchema(global_ints, global_bytes)
    local_schema = transaction.StateSchema(local_ints, local_bytes)

    #to test
    # f = open('./contract/local_contract_clear_stateful.teal').read()
    f = open(approval_file).read()

    approval_program_compiled = compileProgram(acl, f)
    
    # f = open('./contract/local_contract_clear_state.teal').read()
    f = open(clear_file).read()
    clear_state_program_compiled = compileProgram(acl, f)


    # create unsigned transaction
    txn = transaction.ApplicationCreateTxn(sender, params, on_complete, \
                                            approval_program_compiled, clear_state_program_compiled, \
                                            global_schema, local_schema)

    return txn

def generate_delete_app(acl, addr, app_id):
    # get node suggested parameters
    params = acl.suggested_params()
    txn = transaction.ApplicationDeleteTxn(addr, params, app_id)
    return txn

def generate_payment_transaction(acl, sender: str, receiver: str, note: str, amount: int) :

    params = acl.suggested_params()   
    note = encodeBase64(note)
    
    return transaction.PaymentTxn(sender, params, receiver, amount, None, note)

def generate_sc_optin_transaction(acl, sender: str, appid: int) :

    params = acl.suggested_params()   
    
    return transaction.ApplicationOptInTxn(sender, params, appid)

def generate_sc_call_with_args_transaction(acl, sender: str, appid: int, args):

    params = acl.suggested_params()
    on_complete = transaction.OnComplete.NoOpOC.real
      
    
    return transaction.ApplicationCallTxn(sender, params, on_complete=on_complete, index=appid, app_args=args)

def generate_sc_call_with_external_account_args_transaction(acl, sender: str, appid: int, args, accounts):

    params = acl.suggested_params()
    on_complete = transaction.OnComplete.NoOpOC.real
      
    return transaction.ApplicationCallTxn(sender, params, on_complete=on_complete, index=appid, accounts=accounts,  app_args=args)

def generate_sc_optout_transaction(acl, sender, index):
    # create unsigned transaction
    params = acl.suggested_params()
    txn = transaction.ApplicationCloseOutTxn(sender, params, index)
    return txn
    

def create_multisig(threshold, account_list):
    # create a multisig account
    version = 1  # multisig version
    return transaction.Multisig(version, threshold, account_list)

def send_unsigned_single_transaction(acl, txn, sk):

    # sign transaction
    signed_txn = txn.sign(sk)
    tx_id = signed_txn.transaction.get_txid()

    # send transaction
    acl.send_transactions([signed_txn])

    # await confirmation
    wait_for_tx_confirmation(acl, tx_id)

    # display results
    transaction_response = acl.pending_transaction_info(tx_id)
    return transaction_response


def send_signed_transaction(acl, signed_txn):

    # send transaction
    tx_id = acl.send_transactions(signed_txn)

    # await confirmation
    wait_for_tx_confirmation(acl, tx_id)

    # display results
    transaction_response = acl.pending_transaction_info(tx_id)
    return transaction_response

def send_multisigned_transaction(acl, mtx):

    # send the transaction
    tx_id = acl.send_raw_transaction(
            encoding.msgpack_encode(mtx))

    # await confirmation
    wait_for_tx_confirmation(acl, tx_id)

    # display results
    transaction_response = acl.pending_transaction_info(tx_id)
    return transaction_response

def encodeBase64(message):   
    try: #success if message is the an Algorand address 
        message_bytes = encoding.decode_address(message)
        base_64_bytes = base64.b64encode(message_bytes)
        base64_message = base_64_bytes.decode('ascii')
        return base64_message

    except: # string is NOT the encoding of an Algorand address 
        message_bytes = message.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_message = base64_bytes.decode('ascii')
        return base64_message



def decodeBase64(base64_message):
    base64_bytes = base64_message.encode('ascii')
    decoded_bytes = base64.b64decode(base64_bytes)
    
    try: #success if base64_message is the encoding of an Algorand address 
        message = encoding.encode_address(decoded_bytes)
        return message
    
    except: # base64_message is NOT the encoding of an Algorand address 
        message = decoded_bytes.decode('ascii')
        return message


def print_local_state(addr_cmo_2, acl, appid):
    apps = acl.account_info(addr_cmo_2)['apps-local-state']
    
    for app in apps: 
            if app['id'] == appid:
                for pair in app['key-value']:
                        # print(pair)
                    if pair['value']['type'] == 1: print(decodeBase64(pair['key']), decodeBase64(pair['value']['bytes']))
                    elif pair['value']['type'] == 2: print(decodeBase64(pair['key']) , pair['value']['uint'])



def update_manager_addr (acl, sender_addr, sender_sk, new_manager_addr, app_id):
    if not encoding.is_valid_address(new_manager_addr): #TODO1: It would be nice to do this check also in the contract code
        print("not a valid address")
        return
        
    params = acl.suggested_params()
    # declare on_complete as NoOp perché?
    on_complete = transaction.OnComplete.NoOpOC.real
    

    '''Txn.sender = current manager (expected success)'''
    
    
    #https://py-algorand-sdk.readthedocs.io/en/latest/algosdk/future/transaction.html#algosdk.future.transaction.ApplicationCallTxn
    #inputs must be encoded to bytes. Otherwise error is triggered.
    #We pass the public key (encoding.decode_address(new_manager_addr)), because TEAL works with
    #public keys rather than addresses [e.g. Txn.sender(), with which the manager field is compared, 
    #returns the byte32 public key of the tx sender]
    txn = transaction.ApplicationCallTxn(sender_addr, params, on_complete=on_complete, index=app_id,\
                                                 app_args = [str.encode("update_manager"), \
                                                             encoding.decode_address(new_manager_addr)])

    #"add_trusted_pk", pk_to_add, value_to_add
    
    # sign transaction
    signed_txn = txn.sign(sender_sk)
    tx_id = signed_txn.transaction.get_txid()

    # send transaction
    acl.send_transactions([signed_txn])

    # await confirmation
    wait_for_tx_confirmation(tx_id)

    # display results
    transaction_response = acl.pending_transaction_info(tx_id)
    
    print("updated")
    
#for i in range(64):
'''account_1 desigs account_2 as new manager'''

# read user local state
def read_local_state(client, addr, app_id) :   
    results = client.account_info(addr)
    local_state = results['apps-local-state'][0]
    for index in local_state :
        if local_state[index] == app_id :
            print(f"local_state of account {addr} for app_id {app_id}: ", local_state['key-value'])

# read app global state
def read_global_state(client, addr, app_id) :   
    results = client.account_info(addr)
    apps_created = results['created-apps']
    for app in apps_created :
        if app['id'] == app_id :
            print(f"global_state for app_id {app_id}: ", app['params']['global-state'])
