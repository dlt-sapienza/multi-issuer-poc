# Multi-Issuer PoC

## Local variable PoC

Preliminary operation:

``` bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

To compile the contract execute:

```bash
$ python contract/local_contract.py
```

To execute the Proof of Concept with *local variables* execute:
```
$ python main.py
```
